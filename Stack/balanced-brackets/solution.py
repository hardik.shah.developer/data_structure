def balancedBrackets(string):
    if len(string) < 2:
        return False

    openBrackets  = ['(', '{', '[']
    closeBrackets = [')', '}', ']']
    matchBrackets = {')': '(', '}':'{', ']':'['}
    stack = []

    for char in string:
        if char in openBrackets:
            stack.append(char)
        else:
            if char in closeBrackets:
                if len(stack) == 0:
                    return False

                if stack[-1] == matchBrackets[char]:
                    stack.pop()
                else:
                    return False

    if len(stack) == 0:
        return True
    else:
        return False

                


