def sunsetViews(buildings, direction):
    buildingsWithSunsetViews = []

    if direction == 'EAST':
        startIndex = 0
        counter = 1
    else:
        startIndex = len(buildings) - 1
        counter = -1

    index = startIndex
    while index >= 0 and index < len(buildings):
        currentBuildingHeight = buildings[index]

        while len(buildingsWithSunsetViews) > 0 and buildings[buildingsWithSunsetViews[-1]] <= currentBuildingHeight:
            buildingsWithSunsetViews.pop()

        buildingsWithSunsetViews.append(index)

        index = index + counter

    if direction == "EAST":
        return buildingsWithSunsetViews
    else:
        return buildingsWithSunsetViews[::-1]
