class stack:
	def __init__(self):
		self.stack = []

	def add(self, data):
		if data not in self.stack:
			self.stack.append(data)
		else:
			print("data already in stack")

	def peek(self):
		if len(self.stack) > 0:
			print(self.stack[-1])
		else:
			print("No element in the Stack")	

	def remove(self):
		if len(self.stack) <= 0:
			print("No element in the Stack")
		else:
			self.stack.pop()

	def show(self):
		print(self.stack)


s = stack()
print("Adding 5 to stack")
s.add(5)
s.show()

print("Adding 10 to stack")
s.add(10)
s.show()

print("Peek from stack")
s.peek()

print("Adding 15 to stack")
s.add(15)
s.show()

print("Adding 20 to stack")
s.add(20)
s.show()

print("Peek from Stack")
s.peek()

print("Remove from stack")
s.remove()
s.show()

