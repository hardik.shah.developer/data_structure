class mixMaxStack:

	def __init__(self):
		self.minStack = []
		self.maxStack = []
		self.stack = []

	def push(self, number):
		self.stack.append(number)
		if len(self.maxStack) > 0:
			currentMaxValue = self.maxStack[len(self.maxStack) - 1]
			if number > currentMaxValue:
				self.maxStack.append(number)
			else:
				self.maxStack.append(currentMaxValue)
		else:
			self.maxStack.append(number)

		if len(self.minStack) > 0:
			currentMinValue = self.minStack[len(self.minStack) - 1]
			if number < currentMinValue:
				self.minStack.append(number)
			else:
				self.minStack.append(currentMinValue)
		else:
			self.minStack.append(number)

	def show(self):
		print("Stack")
		print(self.stack)
		print("Min Stack")
		print(self.minStack)
		print("Max Stack")
		print(self.maxStack)

	def pop(self):
		self.minStack.pop()
		self.maxStack.pop()
		return self.stack.pop()

	def peek(self):
		return self.stack[len(self.stack) - 1]

	def getMin(self):
		return self.minStack[len(self.minStack) - 1]

	def getMax(self):
		return self.maxStack[len(self.maxStack) - 1]


ins = mixMaxStack()

ins.push(5)
ins.push(7)
ins.push(2)
ins.show()
