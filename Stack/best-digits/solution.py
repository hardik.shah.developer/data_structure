def bestDigits(number, numDigits):
    stack = []

    for digit in number:
        if len(stack) == 0:
            stack.append(digit)
        else:
            while numDigits > 0 and len(stack) > 0 and digit > stack[-1]:
                stack.pop()
                numDigits -= 1

            stack.append(digit)

    while numDigits > 0:
        stack.pop()
        numDigits -= 1

    return "".join(stack)

