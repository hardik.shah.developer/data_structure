<?php

function binary_search($input, $target) {
	$left = 0;
	$right = count($input) - 1;

	while($left <= $right) {
		$middle = ($left + $right) / 2;
		$value = $input[$middle];
		
		if($value == $target) {
			return true;
		}
		else if($target < $value){
			$right = $middle - 1;
		}
		else {
			$left = $middle + 1;
		}
	}
	return false;
}

$input = array(0,1,21,33,45,45,61,71,72,73);
$target = 33;

$output = binary_search($input, $target);
if($output) {
	echo "Found";
}
else {
	echo "Not Found";
}
?>