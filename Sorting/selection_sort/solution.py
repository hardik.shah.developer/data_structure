def selectionSort(array):
	for i in range(0, len(array), 1):
		for j in range(i + 1, len(array), 1):
			minimum = array[i]
			if array[j] < minimum:
				minimum = array[j]
				minimum_key = j

			if minimum != array[i]:
				temp = array[i]
				array[i] = array[minimum_key]
				array[minimum_key] = temp
				print(array)

	return array
			
selectionSort([2,3,4,1,8,5,6]);
