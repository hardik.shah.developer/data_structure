def insertionSort(array):
	for i in range(1, len(array), 1):
		for j in range(i, 0, -1):
			if array[j] < array[j-1]:
				temp = array[j]
				array[j] = array[j-1]
				array[j-1] = temp
				print(array)
	return array

insertionSort([2,3,4,1,8,5,6]);
