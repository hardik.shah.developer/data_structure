def bubbleSort(array):
	for i in range(len(array) - 1, -1, -1):
		for j in range(0, i, 1):
			if array[j] > array[j+1]:
				temp = array[j]
				array[j] = array[j+1]
				array[j+1] = temp
	return array			


array = bubbleSort([2,3,4,1,8,5,6]);
print(array)