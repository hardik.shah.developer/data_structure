<?php
$input = 'abcdcbab';
$output = isPalindrome($input);

if($output) {
	echo "Is Palindrome\n";
}
else {
	echo "Is not Palindrome\n";
}

function isPalindrome($input) {
	$input_str_arrary = str_split($input);
	$leftIndex = 0;
	$rightIndex = count($input_str_arrary) - 1;

	while($leftIndex < $rightIndex) {
		if($input_str_arrary[$leftIndex] != $input_str_arrary[$rightIndex]) {
			return false;
		}
		$leftIndex += 1;
		$rightIndex -= 1;
	}

	return true;
}
?>