def isPalindrome(string):
	leftIndex = 0;
	rightIndex = len(string) - 1;

	while(leftIndex < rightIndex):
		if string[leftIndex] != string[rightIndex]:
			return False

		leftIndex += 1
		rightIndex -= 1

	return True	

string = 'abcdcba'
result = isPalindrome(string)

if result == True:
	print("Is Palindrome")
else:
	print("Is not Palindrome")