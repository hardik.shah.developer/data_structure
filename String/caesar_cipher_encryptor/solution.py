def caesarCipherEncryptor(string, key):
	alphabets = list("abcdefghijklmnopqrstuvwxyz")
	new_string = ""

	for char in string:
		alpha_key = alphabets.index(char)
		shifted_key = alpha_key + key 
		
		if shifted_key < len(alphabets):
			new_key = shifted_key
		else:
			new_key = shifted_key % len(alphabets)
		
		new_char = alphabets[new_key]
		new_string += new_char
	
	return new_string	

result = caesarCipherEncryptor("xyz", 2)
print(result)