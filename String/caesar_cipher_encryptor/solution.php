<?php
$string = 'abd';
$key = 52;

$result = caesarCipherEncryptor($string, $key);
echo $result;

function caesarCipherEncryptor($string, $key) {
	$alphabets = range("a", "z");
	$new_string = "";

	$split_str = str_split($string);
	foreach($split_str as $char) {
		$alpha_key = array_search($char, $alphabets);
		$shifted_key  = $alpha_key + $key;
		
		if($shifted_key  <  count($alphabets)) {
			$new_key = $shifted_key;
		}
		else {
			$new_key = $shifted_key % count($alphabets);
		}
		
		$new_char = $alphabets[$new_key];
		$new_string .= $new_char;
	}
	
	return $new_string;
}
?>