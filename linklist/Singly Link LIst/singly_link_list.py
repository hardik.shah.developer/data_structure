class Node:
	def __init__(self, data):
		self.data = data
		self.next = None					

class SLinkList:
	def __init__(self):
		self.head = None

	def addLast(self, data):
		newNode = Node(data)
		if self.head is None:
			self.head = Node(data)
		else:
			current = self.head
			while current.next:
				current = current.next
			current.next = newNode
			
	def addBegin(self, data):
		newNode = Node(data)
		newNode.next = self.head
		self.head = newNode	

	def printList(self):
		printNode = self.head
		while printNode is not None:
			print(printNode.data)
			printNode = printNode.next	

	def searchNode(self, data):
		nodefound = 0
		current = self.head
		if current.data == data:
			nodefound = 1
		else:
			while current.next:
				current = current.next
				if current.data == data:
					nodefound = 1
					break
		if nodefound == 1:
			print("Node Found")
		else:		
		 	print("Node not found")					

	def addBefore(self, data, existingData):
	 	newNode = Node(data)
	 	nodefound = 0

	 	current = self.head
	 	if current.data == existingData:
	 		newNode.next = current
	 		self.head = newNode
	 	else:
	 		while current.next:
	 			left = current
	 			current = current.next
	 			if current.data == existingData:
	 				newNode.next = current
	 				left.next = newNode
	 				nodefound = 1
	 				break
	 			
	 	if nodefound == 0:
	 		print("Node not found")		


	def deleteNode(self, data):
		current = self.head
		nodefound = 0
		
		if current.data == data:
			self.head = current.next
			current.next = None
		else:
			while current.next:
				left = current
				current = current.next
				if current.data == data:
					left.next = current.next
					current.next = None
					nodefound = 1
					break

		if nodefound == 0:
	 		print("Node not found")	

print("Create Linklist")
l = SLinkList()

print("Add 10 last in Linklist")
l.addLast(10)
l.printList()

print("Add 20 last in Linklist")
l.addLast(20)
l.printList()

print("Add 30 last in Linklist")
l.addLast(30)
l.printList()

print("Add 5 begin of Linklist")
l.addBegin(5)
l.printList()

print("Print Linklist")
l.printList()

print("Delete 30 from linklist")
l.deleteNode(30)
l.printList()

print("Add 50 Before existing Node 25")
l.addBefore(50, 25)
l.printList()

print("Add 50 Before existing Node 20")
l.addBefore(50, 20)
l.printList()

print("Search 20 in linklist")
l.searchNode(20)

print("Search 51 in linklist")
l.searchNode(51)