def sweetAndSavory(dishes, target):
    sweet = []
    sevory = []
    for dish in dishes:
        if dish < 0:
            sweet.append(dish)
        else:
            sevory.append(dish)
    sweet.sort(reverse=True)
    sevory.sort()

    sweetIndex = 0
    sevoryIndex = 0
    bestPairCombination = [0, 0]
    bestDifference = float("inf")

    while sweetIndex < len(sweet) and sevoryIndex < len(sevory):
        taste = sweet[sweetIndex] + sevory[sevoryIndex]

        if taste <= target:
            pairDifference = target - taste

            if pairDifference < bestDifference:
                bestDifference = pairDifference
                bestPairCombination =[sweet[sweetIndex], sevory[sevoryIndex]]
            sevoryIndex += 1
        else:
            sweetIndex += 1
            

    
    return bestPairCombination