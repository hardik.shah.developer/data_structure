def arrayOfProducts(array):
    output = []
    for i in range(len(array)):
    	result = 1;
    	for j in range(len(array)):
    		if i != j:
    			result = result * array[j]
    	output.append(result)

    return output		
    

array = [5, 1, 4, 2]
output = arrayOfProducts(array)   
print(output) 