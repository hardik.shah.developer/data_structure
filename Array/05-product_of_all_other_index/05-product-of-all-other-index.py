

def solutionWithDivision(data):
	result = []
	total = 1
	for x in data:
		total = total * x

	for x in data:
		result.append(total / x)

	return result	

def solutionWithoutDivision(data):
	result = []
	total = 1

	for x in data:
		for y in data:
			if x == y:
				pass
			else:
				total = total * y 

		result.append(total)

	return result	
			
data = [1, 2, 3, 4, 5]

print("Result with Division :: ")
result = solutionWithDivision(data)
print(result)

print("Result without Division :: ")
result = solutionWithDivision(data)
print(result)