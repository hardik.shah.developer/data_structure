def firstDuplicateValue(array):
    minIndex = len(array)
    for i in range(len(array)):
        for j in range(i + 1, len(array)):
            if array[i] == array[j]:
                minIndex = min(minIndex, j)
    if minIndex < len(array):
        return array[minIndex]
    else:
        return -1