def solution(data, k):
	result = []
	for x in data:
		if x <= k:
			remain = k - x
			found = data.index(remain)
			if found > 0:
				result.append(x)
				result.append(remain)
				return result
	return result

data = [10, 15, 3, 7]
k = 17
result = solution(data, k)
print(result)
