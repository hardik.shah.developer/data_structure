
def isValidSubsequence(data, sequence):
	dataIndex = 0
	sequenceIndex = 0
	
	while dataIndex < len(data) and sequenceIndex < len(sequence):
		if data[dataIndex] == sequence[sequenceIndex]:
			sequenceIndex += 1
			dataIndex += 1
		else:
			dataIndex += 1
			
	if sequenceIndex == len(sequence):
		return True
	else:
		return False

data = [5,1,22,25,6,-1,8,10]
sequence = [1,6,-1,10]

valid = isValidSubsequence(data, sequence)
if valid == True:
	print("Valid Subsequence")
else:
	print("Invalid")	