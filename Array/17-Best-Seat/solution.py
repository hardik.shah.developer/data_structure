def bestSeat(seats):
    if len(seats) == 0:
        return -1

    bestSeat = -1
    maxSpace = 0

    left = 0

    while left < len(seats):
        right = left + 1
        while right < len(seats) and seats[right] == 0:
            right = right + 1

        availableSpace = right - left - 1
        if availableSpace > maxSpace:
            maxSpace = availableSpace
            bestSeat = (left + right) // 2

        left = right

    return bestSeat