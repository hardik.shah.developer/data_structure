
def transposeMatrix(matrix):
    output = []
    for column in range(len(matrix[0])):
        newrow = []
        for row in range(len(matrix)):
            newrow.append(matrix[row][column])
        output.append(newrow)
    return output

matrix = [
    [1, 2],
    [3, 4],
    [5, 6]
]

answer = transposeMatrix(matrix)
print(answer)