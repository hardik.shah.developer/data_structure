
def nonConstructibleChange(coins):
    coins.sort()
    chnageTotal = 0

    for coin in coins:
        if coin > chnageTotal + 1:
            return chnageTotal + 1

        chnageTotal += coin

    return chnageTotal + 1

coins = [5, 7, 1, 1, 2, 3, 22]
answer = nonConstructibleChange(coins)
print(answer)