# o(n) time | o(n) space
def twoNumbersum(data, total):
	for num in data:
		otherNum = total - num 
		if otherNum in data:
			return[num, otherNum]

data = [3, 5, -4, 8, 11, 1, -1, 6]
total = 10
result = twoNumbersum(data,total)	
print(result)
