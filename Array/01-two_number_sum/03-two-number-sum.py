# o(nlog(n)) | o(1) space
def twoNumbersum(data, total):
	data.sort()
	left = 0
	right = len(data) - 1
	while left < right:
		totalSum = data[left] + data[right]
		if totalSum == total:
			return [data[left], data[right]]
		elif totalSum < total:
			left += 1
		else:
			right -= 1
	return []		 		

data = [3, 5, -4, 8, 11, 1, -1, 6]
total = 10
result = twoNumbersum(data,total)	
print(result)