
def solution(data):
	m = max(data)
	l = len(data)

	if(m < 1):
		return 0

	if(l == 1):
		if(data[0] == 1):
			return 2
		else:
			return 1

	temp = [0] * m	
	
	for i in data:
		if(i > 0):
			temp[i-1] = 1

	for index, value in enumerate(temp):
		if(value == 0):
			return index + 1

	return len(temp) + 1


data = [1, 2, 0]
result = solution(data)
print(result)