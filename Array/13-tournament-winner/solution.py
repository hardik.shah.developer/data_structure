
def tournamentWinner(competitions, results):
    score = {}
    currentWinTeam = ''
    currentWinTeamScore = 0

    for i in range(len(competitions)):
        hometeam = competitions[i][0]
        awayteam = competitions[i][1]
        if hometeam not in score:
            score[hometeam] = 0
        if awayteam not in score:
            score[awayteam] = 0

        if results[i] == 1:
            score[hometeam] += 3
        else:
            score[awayteam] += 3 

    for team in score:
        if score[team] > currentWinTeamScore:
            currentWinTeamScore = score[team]
            currentWinTeam = team
    return currentWinTeam








competitions = [
  ["HTML", "Java"],
  ["Java", "Python"],
  ["Python", "HTML"]
]

results = [0, 1, 1]

winner = tournamentWinner(competitions, results)
print(winner)