def majorityElement(array):
    result = None
    count = 0

    for value in array:
        if count == 0:
            result = value

        if value == result:
            count += 1
        else:
            count -= 1
    
    return result