def moveElementToEnd(array, toMove):
	left = 0
	right = len(array) - 1
	while left < right:
		if array[left] == toMove:
			if array[left] == array[right]:
				right -= 1
			temp = array[left]
			array[left] = array[right]
			array[right] = temp
		else:
			left += 1
	return array

array = [2, 1, 2, 2, 2, 3, 4, 2]
toMove = 2
result = moveElementToEnd(array, toMove)
print(result)