def longestPeak(array):
	peakLength = 0
	for i in range(1, len(array) - 1):
		if array[i-1] < array[i] and array[i] > array[i+1]:
			peak = [array[i-1], array[i], array[i+1]]
			
			k = i - 1
			while k > 0:
				if array[k - 1] < array[k]:
					peak.insert(0, array[k - 1])
					k -= 1
				else:
					break

			j = i + 1
			while j < len(array) - 1:
				if array[j] > array[j + 1]:
					peak.append(array[j+1])
					j += 1
				else:
					break	

			if len(peak) > peakLength:
				peakLength = len(peak)
				longestpeak = peak
	return longestpeak			
					


array = [1, 2, 3, 3, 4, 0, 10, 6, 5, -1, -3, 2, 3]
result = longestPeak(array)
print(result)

array = [1, 2, 3, 4, 5, 6, 5, 4, 3, 2, 1]
result = longestPeak(array)
print(result)