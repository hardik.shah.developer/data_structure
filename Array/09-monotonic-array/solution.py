'''
An array is monotonic if it is either monotone increasing 
or monotone decreasing
'''
def isMonotonic(array):
	decresing = True
	incresing = True
    for i in range(len(array) - 1):
		if array[i] < array[i+1]:
			decresing = False
		if array[i] > array[i+1]:
			incresing = False
	return incresing or decresing