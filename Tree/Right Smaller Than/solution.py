def rightSmallerThan(array):
	output = []
    for i in range(0, len(array)):
		rightSmallerCounter = 0
		for j in range(i + 1, len(array)):
			if array[j] < array[i]:
				rightSmallerCounter += 1
		output.append(rightSmallerCounter)
	return output

array = [8, 5, 11, -1, 3, 4, 2]
output = rightSmallerThan(array)
print(output)