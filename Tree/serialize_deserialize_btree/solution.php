<?php
class Node {
	public function __construct($value) {
		$this->value = $value;
		$this->left  = NULL;
		$this->right = NULL;
	}
}

class binaryTree {

	private $root = NULL;

	private $record;

	public function insert($value) {
		if($this->root == NULL) {
			$this->root = new Node($value);
		}
		else {
			$current_node = $this->root;
			while(true) {
				if($value < $current_node->value) {
					if($current_node->left == NULL) {
						$current_node->left = new Node($value);
						break;
					}
					else {
						$current_node = $current_node->left;
					}
				}
				else if($value >= $current_node->value) {
					if($current_node->right == NULL) {
						$current_node->right = new Node($value);
						break;
					}
					else {
						$current_node = $current_node->right;
					}
				}
			}
		}
	}

	private function preOrderTraversal($node) {
		if(!is_null($node->value)) {
			$this->record[] = $node->value;
		}

		if($node->left) {
			$this->preOrderTraversal($node->left);
		}

		if($node->right) {
			$this->preOrderTraversal($node->right);
		}
	}

	public function serialize($tree = NULL) {
		unset($this->record);
		if($tree == NULL) {
			$tree = $this->root;
		}
		$this->preOrderTraversal($tree);
		return $this->record;
	}

}

$tree = new Node(20);
$tree->left = new Node(8);
$tree->left->left = new Node(4);
$tree->left->right = new Node(12);
$tree->left->right->left = new Node(10);
$tree->left->right->right = new Node(14);
$tree->right = new Node(22);

$binaryTree = new binaryTree();
$serialize_tree = $binaryTree->serialize($tree);
print_r($serialize_tree);

foreach($serialize_tree as $value) {
	$binaryTree->insert($value);
}

$serialize_tree = $binaryTree->serialize();
print_r($serialize_tree);


?>