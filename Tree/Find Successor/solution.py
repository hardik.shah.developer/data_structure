class Node:
	def __init__(self, value):
		self.value = value
		self.left  = None
		self.right = None

class FindSuccessor:
	def get(self, tree, node):
		order = self.inOrderTraversal(tree)

		for idx, currentNode in enumerate(order):
			if currentNode.value == node.value and currentNode.left == node.left and currentNode.right == node.right:
				if idx < len(order):
					successor = order[idx + 1]
					return successor.value

		return None			

	def inOrderTraversal(self, tree, order=[]):
		if tree is None:
			return order

		if tree.left:
			self.inOrderTraversal(tree.left, order)

		order.append(tree)
		
		if tree.right:
			self.inOrderTraversal(tree.right, order)

		return order		


Tree = Node(1)
Tree.left = Node(2)
Tree.right = Node(3)
Tree.left.left = Node(4)
Tree.left.right = Node(5)		
Tree.left.left.left = Node(6)

node = Node(5)

findSuccessor = FindSuccessor()
successor =  findSuccessor.get(Tree, node)
print(successor)