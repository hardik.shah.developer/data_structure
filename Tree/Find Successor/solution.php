<?php
class Node {
	public function __construct($value) {
		$this->value = $value;
		$this->left  = NULL;
		$this->right = NULL;
	}
}

class FindSuccessor {

	private $order;

	public function get($tree, $node) {
		$this->inOrderTranversal($tree);

		foreach($this->order as $idx => $current_node) {
			if($current_node == $node) {
				if($idx < count($this->order) - 1) {
					$successor = $this->order[$idx + 1];
					return $successor->value;
				}
			}
		}

		return "None";
	}

	private function inOrderTranversal($tree) {
		if($tree == NULL) {
			return;
		}

		if($tree->left) {
			$this->inOrderTranversal($tree->left);
		}

		$this->order[] = $tree;

		if($tree->right) {
			$this->inOrderTranversal($tree->right);
		}
	}
}

$tree = new Node(1);
$tree->left = new Node(2);
$tree->right = new Node(3);
$tree->left->left = new Node(4);
$tree->left->right = new Node(5);		
$tree->left->left->left = new Node(6);

$node = new Node(5);

$findSuccessor = new FindSuccessor();
$successor = $findSuccessor->get($tree, $node);
echo $successor;
?>