class Node:
	def __init__(self, value):
		self.value = value
		self.left  = None
		self.right = None

def heightBalancedBinaryTree(tree):
	
	leftTreeHeight = treeHeight(tree.left) if tree.left else 0
	rightTreeHeight = treeHeight(tree.right) if tree.right else 0
	
	if abs(leftTreeHeight - rightTreeHeight) > 1:
		return False
	
	if tree.left:
		heightBalancedBinaryTree(tree.left)
		
	if tree.right:
		heightBalancedBinaryTree(tree.right)
		
	return True

def treeHeight(tree):
	return 1 + max(treeHeight(tree.left) if tree.left else 0, treeHeight(tree.right) if tree.right else 0)
		

Tree = Node(1)
Tree.left = Node(2)
Tree.right = Node(3)
Tree.left.left = Node(4)
Tree.left.right = Node(5)		
Tree.right.right = Node(6)
Tree.left.right.left = Node(7)
Tree.left.right.right = Node(8)

output = heightBalancedBinaryTree(Tree)

if output == True:
	print("Balanced")
else:
	print ("Not Balanced")	