<?php

class Node {
	public function __construct($value) {
		$this->value = $value;
		$this->left  = NULL;
		$this->right = NULL;
	}
}

class unival {

	private $count = 0;

	public function check($tree) {
		$left = false;
		$right = false;
		echo $tree->value." :: ";
		if(!is_null($tree->value)) {
			if((($tree->left) && ($tree->left->value == $tree->value)) || ($tree->left == NULL)) {
				$left = true;
			}

			if((($tree->right) && ($tree->right->value == $tree->value)) || ($tree->right == NULL)) {
				$right = true;
			}
			
			if($left == true && $right == true) {
				$this->count += 1;
			}
		}
		echo $this->count."\n";

		if($tree->left) {
			$this->check($tree->left);
		}

		if($tree->right) {
			$this->check($tree->right);
		}
		
		return $this->count;
	}
}

$tree = new Node(0);
$tree->left = new Node(1);
$tree->right = new Node(0);
$tree->right->left = new Node(1);
$tree->right->right = new Node(0);
$tree->right->left->left = new Node(1);
$tree->right->left->right = new Node(1);

$unival = new unival();
$total = $unival->check($tree);
echo $total;
?>