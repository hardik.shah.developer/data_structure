<?php

class Node {
	public function __construct($value) {
		$this->value = $value;
		$this->left  = NULL;
		$this->right = NULL;
	}
}

class BST {

	private $root = NULL;

	public function insert($value) {
		if($this->root == NULL) {
			$this->root = new Node($value);
		}
		else {
			$current_node = $this->root;
			while(true) {
				if($value < $current_node->value) {
					if($current_node->left == NULL) {
						$current_node->left = new Node($value);
						break;
					}
					else {
						$current_node = $current_node->left;
					}
				}
				else if ($value >= $current_node->value) {
					if($current_node->right == NULL) {
						$current_node->right = new Node($value);
						break;
					}
					else {
						$current_node = $current_node->right;
					}
				}
			}
		}
	}

	public function levelOrderTraversal() {
		$height = $this->getTreeHeight();
		for($i=1; $i<$height+1; $i++) {
			echo "\nLevel ".$i." :: ";
			$this->getLevelNodes($this->root, $i);
		}
	}

	private function getLevelNodes($node, $level) {
		if($node == NULL) {
			return;
		}
		else if($level ==1) {
			echo $node->value." ";
		}
		else {
			$this->getLevelNodes($node->left, $level - 1);
			$this->getLevelNodes($node->right, $level - 1);
		}
	}

	private function getTreeHeight() {
		if($this->root == NULL) {
			return 0;
		}
		return $this->treeHeight($this->root);
	}

	private function treeHeight($node) {
	    if($node == NULL) {
	      return 0;
	    }
	    
	    return 1 + max($this->treeHeight($node->left), $this->treeHeight($node->right));
	}
}

class minHeightBST {

	private $bst;

	public function __construct($array) {
		$this->bst = new BST();
		$this->createMinHeightBST($array, 0, count($array) - 1);
	}

	private function createMinHeightBST($array, $start, $end) {
		if($end >= $start) {
			$mid = FLOOR(($start + $end) / 2);
			$value = $array[$mid];

			$this->bst->insert($value);
			$this->createMinHeightBST($array, $start, $mid - 1);
			$this->createMinHeightBST($array, $mid + 1, $end);
		}
	}	

	public function output() {
		$this->bst->levelOrderTraversal();
	}

}

$array = array(1,2,5,7,10,13,14,15,22);
$ins = new minHeightBST($array);
$ins->output();


?>