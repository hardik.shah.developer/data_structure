class Node:
	def __init__(self, data):
		self.data = data
		self.left = None
		self.right = None

	def insert(self, value):
		if value < self.data:
			if self.left is None:
				self.left = Node(value)
			else:
				self.left.insert(value)

		if value > self.data:
			if self.right is None:
				self.right = Node(value)
			else:
				self.right.insert(value)

	
def validateBst(tree):
	return validate(tree, float("-inf"), float("inf"))	

def validate(Tree, minValue, maxValue):
	if Tree is None:
		return True

	if Tree.value < minValue or Tree.value >= maxValue:
		return False

	checkLeftTree = validate(Tree.left, minValue, Tree.value)
	
	checkRightTree = validate(Tree.right, Tree.value, maxValue)

	return checkLeftTree and checkRightTree	


Tree = Node(10)
Tree.insert(5)
Tree.insert(15)
Tree.insert(5)
Tree.insert(2)
Tree.insert(1)
Tree.insert(22)
Tree.insert(13)
Tree.insert(14)
Tree.insert(3)
	
validateBst(Tree)


