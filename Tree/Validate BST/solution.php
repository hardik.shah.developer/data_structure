<?php
class Node {
	public function __construct($value) {
		$this->value = $value;
		$this->left  = NULL;
		$this->right = NULL;
	}
}

class validateBst {

	public function do_validate($tree) {
		$min_val = '-'.PHP_INT_MAX;
		return $this->is_valid_tree($tree, $min_val, PHP_INT_MAX);
	}

	private function is_valid_tree($tree, $min_val, $max_val) {
		echo $tree->value."\n";
		if($tree == NULL) {
			return true;
		}

		if($tree->value < $min_val || $tree->value >= $max_val) {
			return false;
		}
		
		$checkLeftTree = $this->is_valid_tree($tree->left, $min_val, $tree->value);
		$checkRightTree = $this->is_valid_tree($tree->right, $tree->value, $max_val);

		if(($checkLeftTree) && ($checkRightTree)) {
			return true;
		}

		return false;
	}
}

$tree = new Node(10);
$tree->left = new Node(5);
$tree->right = new Node(15);
$tree->left->left =  new Node(2);
$tree->left->right = new Node(5);
$tree->right->left =  new Node(13);
$tree->right->right = new Node(22);
$tree->left->left->left = new Node(1);
$tree->right->left->right = new Node(14);

$ins = new validateBst();
$output = $ins->do_validate($tree);

if($output) {
	echo "Is BST\n";
}
else {
	echo "No BST\n";
}
	

?>