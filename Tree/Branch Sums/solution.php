<?php
/*
          1
        /   \
      2      3
     / \    /  \
    4   5   6  7
   / \   \      
  8   9   10

  Output = [15, 16, 18, 10, 11]
*/
class Node {
	public function __construct($value) {
		$this->value = $value;
		$this->left = NULL;
		$this->right = NULL;
	}
}

function branchSum($tree, $runningSum = 0) {
	global $sums;

	$runningSum = $runningSum + $tree->value;
	
	if($tree->left == NULL && $tree->right == NULL) {
		$sums[] = $runningSum;
	}

	if($tree->left) {
		branchSum($tree->left, $runningSum, $sums);
	}

	if($tree->right) {
		branchSum($tree->right, $runningSum, $sums);
	}
}

$tree = new Node(1);
$tree->left = new Node(2);
$tree->right = new Node(3);
$tree->left->left = new Node(4);
$tree->left->right = new Node(5);
$tree->right->left = new Node(6);
$tree->right->right = new Node(7);
$tree->left->left->left = new Node(8);
$tree->left->left->right = new Node(9);
$tree->left->right->right = new Node(10);

$sums = array();
branchSum($tree);
print_r($sums);

?>