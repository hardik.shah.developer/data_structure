'''
          1
        /   \
      2      3
     / \    /  \
    4   5   6  7
   / \   \      
  8   9   10

  Output = [15, 16, 18, 10, 11]
'''


def branchSums(tree):
	sums = []
	calculateBranchSums(tree, 0, sums)
	return sums

def calculateBranchSums(node, runningSum, sums):
	if node is None:
		return

	runningSum = runningSum + node.value
	
	if node.left is None and node.right is None:
		sums.append(runningSum)
		return 

	calculateBranchSums(node.left, runningSum, sums)
	calculateBranchSums(node.right, runningSum, sums)


class Node:
	def __init__(self, value):
		self.value = value
		self.left = None
		self.right = None

tree = Node(1)  
tree.left = Node(2)  
tree.right = Node(3)  
tree.left.left = Node(4)  
tree.left.right = Node(5)  
tree.right.left = Node(6) 
tree.right.right = Node(7)  
tree.left.left.left = Node(8)  
tree.left.left.right = Node(9)
tree.left.right.right = Node(10) 	

output = branchSums(tree)
print(output)