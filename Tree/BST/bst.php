<?php

class Node {
	public function __construct($value) {
		$this->value = $value;
		$this->left = NULL;
		$this->right = NULL;
	}
}

class BST {

	private $root;

	public function add($value) {
		if($this->root == NULL) {
			$this->root = new Node($value);
		}
		else {
			$current_node = $this->root;
			while(true) {
				if($value < $current_node->value) {
					if($current_node->left == NULL) {
						$current_node->left = new Node($value);
						break;
					}
					else {
						$current_node = $current_node->left;
					}
				}
				else if($value >= $current_node->value){
					if($current_node->right == NULL) {
						$current_node->right = new Node($value);
						break;
					}
					else {
						$current_node = $current_node->right;
					}
				}
			}
		}
	}

	//Left Root Right
	public function inorderTraversal($node = NULL) {
		if(empty($node)) {
			$node = $this->root;
		}

		if($node->left) {
			$this->inorderTraversal($node->left);
		}

		echo $node->value." ";

		if($node->right) {
			$this->inorderTraversal($node->right);
		}
	}

	//Root Left Right
	public function preOrderTraversal($node = NULL) {
		if(empty($node)) {
			$node = $this->root;
		}

		echo $node->value." ";

		if($node->left) {
			$this->preOrderTraversal($node->left);
		}

		if($node->right) {
			$this->preOrderTraversal($node->right);
		}
	}

	//Left Right Root
	public function postOrderTraversal($node = NULL) {
		if(empty($node)) {
			$node = $this->root;
		}

		if($node->left) {
			$this->postOrderTraversal($node->left);
		}

		if($node->right) {
			$this->postOrderTraversal($node->right);
		}

		echo $node->value." ";
	}

	public function levelOrderTraversal() {
		$height = $this->getTreeHeight();
		for($i=1; $i<$height+1; $i++) {
			echo "\nLevel ".$i." :: ";
			$this->getLevelNodes($this->root, $i);
		}
	}

	public function getNthLevelNodes($level) {
		$height = $this->getTreeHeight();
		if($level > $height) {
			echo "Tree does not have requesting level\n";
		}
		else {
			$this->getLevelNodes($this->root, $level);
		}
	}

	private function getLevelNodes($node, $level) {
		if($node == NULL) {
			return;
		}
		else if($level ==1) {
			echo $node->value." ";
		}
		else {
			$this->getLevelNodes($node->left, $level - 1);
			$this->getLevelNodes($node->right, $level - 1);
		}
	}

	public function contains($value) {
		$current_node = $this->root;
		while($current_node != NULL) {
			if($value < $current_node->value) {
				$current_node = $current_node->left;
			}
			else if($value > $current_node->value) {
				$current_node = $current_node->right;
			}
			else {
				return 1;
			}
		}
		return 0;
	}

	public function getMinNode() {
		$current_node = $this->root;
		while($current_node->left) {
			$current_node = $current_node->left;
		}
		return $current_node->value;
	}

	public function getMaxNode() {
		$current_node = $this->root;
		while($current_node->right) {
			$current_node = $current_node->right;
		}
		return $current_node->value;
	}

	public function getTreeHeight() {
		if($this->root == NULL) {
			return 0;
		}
		return $this->treeHeight($this->root);
	}

	private function treeHeight($node) {
	    if($node == NULL) {
	      return 0;
	    }
	    
	    return 1 + max($this->treeHeight($node->left), $this->treeHeight($node->right));
	}

	public function checkBalanced() {
		$left_height = $this->treeHeight($this->root->left);
		$right_height = $this->treeHeight($this->root->right);
		$difference = abs($left_height - $right_height);
		if($difference > 1) {
			echo  "Not Balanced\n";
		}
		else {
			echo  "Balanced\n";
		}
	}


}

$input = array(12, 6, 14, 3);

$bst = new BST();
foreach($input as $value) {
	$bst->add($value);
}

echo "\nInOrder Tranversal :: ";
$bst->inorderTraversal();

echo "\nPreOrder Tranversal :: ";
$bst->preOrderTraversal()."\n";

echo "\nPostOrder Tranversal :: ";
$bst->postOrderTraversal()."\n";

echo "\nLevelOrder Tranversal :: ";
$bst->levelOrderTraversal()."\n";

echo "\nGet 2nd level Nodes :: ";
$bst->getNthLevelNodes(2);

echo "\nFind if contains  5 :: ";
$output = $bst->contains(5);
echo $output;

echo "\nFind if contains  12 :: ";
$output = $bst->contains(12);
echo $output;

echo "\nMin Node :: ";
$output = $bst->getMinNode();
echo $output;

echo "\nMax Node :: ";
$output = $bst->getMaxNode();
echo $output;

echo "\nTree Height :: ";
$output = $bst->getTreeHeight();
echo $output;

echo "\nCheck Balanced :: ";
$bst->checkBalanced();


?>