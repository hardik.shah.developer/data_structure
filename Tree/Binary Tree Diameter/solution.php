<?php
class Node {

	public function __construct($value) {
		$this->value = $value;
		$this->left = NULL;
		$this->right = NULL;
	}
}

class TreeInfo {

	public function __construct($diameter, $height) {
		$this->diameter = $diameter;
		$this->height = $height;
	}
}

class BinaryTreeDiameter {

	public function getDiameter($tree) {
		$treeInfo = $this->getTreeInfo($tree);
		return $treeInfo->diameter;
	}

	public function getTreeInfo($tree) {
		if(empty($tree)) {
			return new TreeInfo(0,0);
		}

		$leftTreeInfo = $this->getTreeInfo($tree->left);
		$rightTreeInfo = $this->getTreeInfo($tree->right);

		$longestPathThroughRoot = $leftTreeInfo->height + $rightTreeInfo->height;
		$maxDiameterNow = max($leftTreeInfo->diameter, $rightTreeInfo->diameter);
		$currentNodeDiameter = max($longestPathThroughRoot, $maxDiameterNow);
		$currentNodeHeight = 1 + max($leftTreeInfo->height, $rightTreeInfo->height);

		$treeInfo = new TreeInfo($currentNodeDiameter, $currentNodeHeight);

		return $treeInfo;
	}
}



$tree = new Node(1);
$tree->left = new Node(3);
$tree->right = new Node(2);
$tree->left->left = new Node(7);
$tree->left->right = new Node(4);
$tree->left->left->left = new Node(8);
$tree->left->right->right = new Node(5);
$tree->left->left->left->left = new Node(9);
$tree->left->right->right->right = new Node(6);

$ins = new BinaryTreeDiameter();
$binaryTreeDiameter = $ins->getDiameter($tree);
echo $binaryTreeDiameter;


?>