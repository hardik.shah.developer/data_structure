def sameBsts(arrayOne, arrayTwo):
    if len(arrayOne) != len(arrayTwo):
		return False
	
	if len(arrayOne) == 0 and len(arrayTwo) == 0:
		return True
	
	if arrayOne[0] != arrayTwo[0]:
		return False
	
	arrayOneLeft = getSmaller(arrayOne)
	arrayOneRight = getGreaterOrEqual(arrayOne)
	arrayTwoLeft = getSmaller(arrayTwo)
	arrayTwoRight = getGreaterOrEqual(arrayTwo)
	
	return sameBsts(arrayOneLeft, arrayTwoLeft) and sameBsts(arrayOneRight, arrayTwoRight)
	
	
	
def getSmaller(array):
	smaller = []
	for i in range(1, len(array)):
		if array[i] < array[0]:
			smaller.append(array[i])
	return smaller

def getGreaterOrEqual(array):
	greaterOrEqual = []
	for i in range(1, len(array)):
		if array[i] >= array[0]:
			greaterOrEqual.append(array[i])
	return greaterOrEqual


arrayOne = [10, 15, 8, 12, 94, 81, 5, 2, 11]
arrayTwo = [10, 8, 5, 15, 2, 12, 11, 94, 81]

output = sameBsts(arrayOne, arrayTwo)
