<?php
class Node {
	public function __construct($value) {
		$this->value = $value;
		$this->left = null;
		$this->right = null;
	}
}

function findClosest($node, $target, $closest) {
	if(empty($node)) {
		return $closest;
	}

	if(abs($target - $closest) > abs($target - $node->value)) {
		$closest = $node->value;
	}

	if($target < $node->value) {
		$closest = findClosest($node->left, $target, $closest);
	}
	else {
		$closest = findClosest($node->right, $target, $closest);
	}

	return $closest;
}

$tree = new Node(10);
$tree->left = new Node(5); 
$tree->right = new Node(15);  
$tree->left->left = new Node(2); 
$tree->left->right = new Node(5);  
$tree->right->left = new Node(13); 
$tree->right->right = new Node(22);  
$tree->left->left->left = new Node(1);  
$tree->right->left->right = new Node(14); 

$target = 21;
$closest = $tree->value;
$result = findClosest($tree, $target, $closest);
echo $result;

?>