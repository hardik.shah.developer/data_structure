def findClosest(tree, target, closest):
	if tree is None:
		return closest

	if abs(target - closest) > abs(target - tree.value):
		closest = tree.value

	if target < tree.value:
		return findClosest(tree.left, target, closest)
	elif target > tree.value:
		return findClosest(tree.right, target, closest)
	else:
		return closest	

		
class Node:
	def __init__(self, value):
		self.value = value
		self.left = None
		self.right = None

tree = Node(10)  
tree.left = Node(5)  
tree.right = Node(15)  
tree.left.left = Node(2)  
tree.left.right = Node(5)  
tree.right.left = Node(13) 
tree.right.right = Node(22)  
tree.left.left.left = Node(1)  
tree.right.left.right = Node(14) 

target = 21
result = findClosest(tree, target, tree.value)
print(result)
